import { Component, OnInit, ViewChild, ViewEncapsulation  } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Chart } from 'chart.js';
import * as moment from 'moment';

import { GetDataService } from '../_services/get-data.service';
import { DataContainerService } from '../_services/data-container.service';
import { AggTimeByUniqueFile, AggTimeByFolderLocation } from '../_services/_interfaces/data-formats';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-tests',
  templateUrl: './tests.component.html',
  styleUrls: ['./tests.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TestsComponent implements OnInit {
  @ViewChild('f') loginForm: NgForm;
  DateRange = {
    startDate: Date,
    endDate: Date
  };
  aggByTimeAndFile: AggTimeByUniqueFile;
  startDate: Date;
  endDate: Date;

  chartData = [];
  barCharData = [];
  linechartData = [];
  top5_right = [];
  top5_left = [];
  colors = {
    CruiseControlFE_v1: '#ff0000',
    CruiseControlFE_v2: '#ff0000',
    cdr_v1: '#00ffff',
    cdr_v2: '#00ffff'
  };

  constructor(
    private dataService: DataContainerService,
    private _getData: GetDataService,
    private httpService: HttpClient) { }

  ngOnInit() {
    const currDate = new Date(moment().format('YYYY-MM-DD'));
    const firstDayOfMonth = new Date(moment().startOf('month').format('YYYY-MM-DD'));
    this._getData.getTestDataOne(firstDayOfMonth, currDate)
    .subscribe(data => {
      console.log(this.dataService.totalTimeByFile);
    });
    this.dataService.totalTimeByFileUpdated
      .subscribe(
        (totalTimeByFile: AggTimeByUniqueFile) => {
          this.aggByTimeAndFile = totalTimeByFile;
        }
      );

    this.httpService.get('./assets/dummyData.json').subscribe(
      data => {
        this.chartData = data as string[];	 // FILL THE ARRAY WITH DATA.
        console.log(this.chartData);
        this.initChart();
        this.piechartRight();
        this.piechartLeft();
      },
      (err: HttpErrorResponse) => {
        console.log(err.message);
      }
    );

  }

  onSubmitDateRange() {
    this.startDate = this.loginForm.value.DateRange.startDate;
    this.endDate = this.loginForm.value.DateRange.endDate;
    console.log(this.startDate);
    console.log(this.endDate);
  }

  type(d) {
    d.frequency = +d.frequency;
    return d;
  }

  initChart() {
    var me = this;
    document.getElementById('d3-chart-div').innerHTML = '';
    this.linechartData = [];
    this.startDate = this.loginForm.value.DateRange.startDate;
    this.endDate = this.loginForm.value.DateRange.endDate;
    var parseDate = d3.time.format('%Y%m%d').parse;
    this.chartData.map(dateData => {
      var CruiseControlFE_v1 = 0;
      var CruiseControlFE_v2 = 0;
      var cdr_v1 = 0;
      var cdr_v2 = 0;
      dateData.AnalyzedDataArray.map(projectData => {
        me.top5_right.push({
          Date: dateData.Date,
          ProjectName: projectData.ProjectName,
          fileName: projectData.fileName,
          filePath: projectData.filePath,
          value1: projectData.value1,
          value2: projectData.value2
        });
        if (projectData.ProjectName === 'CruiseControlFE') {
          CruiseControlFE_v1 += projectData.value1;
          CruiseControlFE_v2 += projectData.value2;
        } else {
          cdr_v1 += projectData.value1;
          cdr_v2 += projectData.value2;
        }
      });

      if ((me.startDate === '' && me.endDate === '') ||
        (me.startDate === '' && me.endDate !== '' && dateData.Date <= me.endDate) ||
        (me.startDate !== '' && me.endDate === '' && dateData.Date >= me.startDate) ||
        (me.startDate !== '' && me.endDate !== '' && dateData.Date >= me.startDate && dateData.Date <= me.endDate)
        ) {
        this.linechartData.push({
          Date: dateData.Date.replace(/-/gi, ''),
          CruiseControlFE_v1: CruiseControlFE_v1,
          CruiseControlFE_v2: CruiseControlFE_v2,
          cdr_v1: cdr_v1,
          cdr_v2: cdr_v2
        });
      }

    });


    var margin = {
      top: 20,
      right: 80,
      bottom: 30,
      left: 50
    },
    width = 900 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

    

    var x = d3.time.scale()
      .range([0, width]);

    var y = d3.scale.linear()
      .range([height, 0]);

    var color = d3.scale.category10();

    var xAxis = d3.svg.axis()
      .scale(x)
      .orient("bottom");

    var yAxis = d3.svg.axis()
      .scale(y)
      .orient("left");

    var line = d3.svg.line()
      .interpolate('basis')
      .x(function (d) {
        return x(d.Date);
      })
      .y(function (d) {
        return y(d.temperature);
      });

    var svg = d3.select('#d3-chart-div').append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var data = this.linechartData;
    console.log(this.linechartData);

    color.domain(d3.keys(data[0]).filter(function (key) {
      return key !== 'Date';
    }));
    console.log(color);
    data.forEach(function (d) {
      d.Date = parseDate(d.Date);
    });

    var cities = color.domain().map(function (name) {
      return {
        name: name,
        values: data.map(function (d) {
          return {
            Date: d.Date,
            temperature: +d[name]
          };
        })
      };
    });

    x.domain(d3.extent(data, function (d) {
      return d.Date;
    }));

    y.domain([
      d3.min(cities, function (c) {
        return d3.min(c.values, function (v) {
          return v.temperature;
        });
      }),
      d3.max(cities, function (c) {
        return d3.max(c.values, function (v) {
          return v.temperature;
        });
      })
    ]);

    var legend = svg.selectAll('g')
      .data(cities)
      .enter()
      .append('g')
      .attr('class', 'legend');

    legend.append('rect')
      .attr('x', width - 20)
      .attr('y', function (d, i) {
        return i * 20;
      })
      .attr('width', 10)
      .attr('height', 10)
      .style('fill', function (d) {
        // return color(d.name);
        return me.colors[d.name];
      });

    legend.append('text')
      .attr('x', width - 8)
      .attr('y', function (d, i) {
        return (i * 20) + 9;
      })
      .text(function (d) {
        return d.name;
      });

    svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);

    svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
      .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("value");

    var city = svg.selectAll(".city")
      .data(cities)
      .enter().append("g")
      .attr("class", "city");

    city.append('path')
      .attr('class', 'line')
      .attr('d', function (d) {
        return line(d.values);
      })
      .style('stroke', function (d) {
        return me.colors[d.name];
      })
      .style('stroke-dasharray', function(d) {
        if (d.name === 'CruiseControlFE_v2' || d.name === 'cdr_v2') {
          return '4,4';
        } else {
          return '0, 0';
        }
      });

    city.append('text')
      .datum(function (d) {
        return {
          name: d.name,
          value: d.values[d.values.length - 1]
        };
      })
      .attr("transform", function (d) {
        return "translate(" + x(d.value.Date) + "," + y(d.value.temperature) + ")";
      })
      .attr("x", 3)
      .attr("dy", ".35em")
      .text(function (d) {
        return d.name;
      });

    var mouseG = svg.append("g")
      .attr("class", "mouse-over-effects");

    mouseG.append("path") // this is the black vertical line to follow mouse
      .attr("class", "mouse-line")
      .style("stroke", "black")
      .style("stroke-width", "1px")
      .style("opacity", "0");

    var lines = document.getElementsByClassName('line');

    var mousePerLine = mouseG.selectAll('.mouse-per-line')
      .data(cities)
      .enter()
      .append("g")
      .attr("class", "mouse-per-line");

    mousePerLine.append("circle")
      .attr("r", 7)
      .style("stroke", function (d) {
        // return color(d.name);
        return me.colors[d.name];
      })
      .style("fill", "none")
      .style("stroke-width", "1px")
      .style("opacity", "0");

    mousePerLine.append("text")
      .attr("transform", "translate(10,3)");

    mouseG.append('svg:rect') // append a rect to catch mouse movements on canvas
      .attr('width', width) // can't catch mouse events on a g element
      .attr('height', height)
      .attr('fill', 'none')
      .attr('pointer-events', 'all')
      .on('mouseout', function () { // on mouse out hide line, circles and text
        d3.select(".mouse-line")
          .style("opacity", "0");
        d3.selectAll(".mouse-per-line circle")
          .style("opacity", "0");
        d3.selectAll(".mouse-per-line text")
          .style("opacity", "0");
      })
      .on('mouseover', function () { // on mouse in show line, circles and text
        d3.select(".mouse-line")
          .style("opacity", "1");
        d3.selectAll(".mouse-per-line circle")
          .style("opacity", "1");
        d3.selectAll(".mouse-per-line text")
          .style("opacity", "1");
      })
      .on('mousemove', function () { // mouse moving over canvas
        var mouse = d3.mouse(this);
        d3.select(".mouse-line")
          .attr("d", function () {
            var d = "M" + mouse[0] + "," + height;
            d += " " + mouse[0] + "," + 0;
            return d;
          });

        d3.selectAll(".mouse-per-line")
          .attr("transform", function (d, i) {
            console.log(width / mouse[0]);
            var xDate = x.invert(mouse[0]);
            var bisect = d3.bisector(function (d) { return d.Date; }).right;
            var idx = bisect(d.values, xDate);

            var beginning = 0,
              end = lines[i].getTotalLength(),
              target = null;

            while (true) {
              var target = Math.floor((beginning + end) / 2);
              var pos = lines[i].getPointAtLength(target);
              if ((target === end || target === beginning) && pos.x !== mouse[0]) {
                break;
              }
              if (pos.x > mouse[0]) end = target;
              else if (pos.x < mouse[0]) beginning = target;
              else break; //position found
            }

            d3.select(this).select('text')
              .text(y.invert(pos.y).toFixed(2));

            return "translate(" + mouse[0] + "," + pos.y + ")";
          });
      });
     
  }

  piechartRight() {

    var me = this;
    var parseDate = d3.time.format("%Y%m%d").parse;
    var width = 360;
    var height = 360;
    var radius = Math.min(width, height) / 2;
    var donutWidth = 75;
    var legendRectSize = 18;
    var legendSpacing = 4;

    var color = d3.scale.category10();

    var svg = d3.select('#piechart-right')
      .append('svg')
      .attr('width', width)
      .attr('height', height)
      .append('g')
      .attr('transform', 'translate(' + (width / 2) +
        ',' + (height / 2) + ')');

    var arc = d3.svg.arc()
      .innerRadius(radius - donutWidth)
      .outerRadius(radius);

    var pie = d3.layout.pie()
      .value(function (d) { return d.value1; })
      .sort(null);

    var tooltip = d3.select('#piechart-right')
      .append('div')
      .attr('class', 'tooltip1');

    tooltip.append('div')
      .attr('class', 'Date');

    tooltip.append('div')
      .attr('class', 'projectName');

    tooltip.append('div')
      .attr('class', 'fileName');

    tooltip.append('div')
      .attr('class', 'value1');

    tooltip.append('div')
      .attr('class', 'value2');

    // d3.csv('weekdays.csv', function (error, dataset) {
    me.top5_right.sort(function (a, b) {
      return parseFloat(b.value1) - parseFloat(a.value1);
    });

    var dataset = me.top5_right.slice(0, 5);

      dataset.forEach(function (d) {
        d.value1 = +d.value1;
      });

      var path = svg.selectAll('path')
        .data(pie(dataset))
        .enter()
        .append('path')
        .attr('d', arc)
        .style('fill', function (d, i) {
          return color(d.data.value1);
        });

      path.on('mouseover', function (d) {
        var total = d3.sum(dataset.map(function (d) {
          return d.value1;
        }));
        var percent = Math.round(1000 * d.data.value1 / total) / 10;
        tooltip.select('.Date').html('date:' + d.data.Date);
        tooltip.select('.projectName').html('project name:' + d.data.ProjectName);
        tooltip.select('.fileName').html('file name:' + d.data.fileName);
        tooltip.select('.value1').html('value1:' + d.data.value1);
        tooltip.select('.value2').html('value2:' + d.data.value2);
        tooltip.style('display', 'block');
      });

      path.on('mouseout', function () {
        tooltip.style('display', 'none');
      });

      var legend = svg.selectAll('.legend')
        .data(color.domain())
        .enter()
        .append('g')
        .attr('class', 'legend')
        .attr('transform', function (d, i) {
          var height = legendRectSize + legendSpacing;
          var offset = height * color.domain().length / 2;
          var horz = -2 * legendRectSize;
          var vert = i * height - offset;
          return 'translate(' + horz + ',' + vert + ')';
        });

      legend.append('rect')
        .attr('width', legendRectSize)
        .attr('height', legendRectSize)
        .style('fill', color)
        .style('stroke', color);

      legend.append('text')
        .attr('x', legendRectSize + legendSpacing)
        .attr('y', legendRectSize - legendSpacing)
        .text(function (d) { return d; });

    // });
  }

  piechartLeft() {

    var me = this;
    var parseDate = d3.time.format("%Y%m%d").parse;
    var width = 360;
    var height = 360;
    var radius = Math.min(width, height) / 2;
    var donutWidth = 75;
    var legendRectSize = 18;
    var legendSpacing = 4;

    var color = d3.scale.category10();

    var svg = d3.select('#piechart-left')
      .append('svg')
      .attr('width', width)
      .attr('height', height)
      .append('g')
      .attr('transform', 'translate(' + (width / 2) + ',' + (height / 2) + ')');

    var arc = d3.svg.arc()
      .innerRadius(radius - donutWidth)
      .outerRadius(radius);

    var pie = d3.layout.pie()
      .value(function (d) { return d.value1; })
      .sort(null);

    var tooltip = d3.select('#piechart-left')
      .append('div')
      .attr('class', 'tooltip1');

    tooltip.append('div')
      .attr('class', 'projectName');

    tooltip.append('div')
      .attr('class', 'filePath');

    tooltip.append('div')
      .attr('class', 'value1');

    // d3.csv('weekdays.csv', function (error, dataset) {
    me.top5_right.sort(function (a, b) {
      return parseFloat(b.value1) - parseFloat(a.value1);
    });

    me.top5_right.map(item => {
      var isExist = false;
      me.top5_left.map(leftitem => {
        if (leftitem.ProjectName === item.ProjectName && JSON.stringify(leftitem.filePath) === JSON.stringify(item.filePath)) {
          leftitem.value1 += item.value1;
          isExist = true;
        } else {
        }
      });

      if (!isExist) {
        this.top5_left.push({
          ProjectName: item.ProjectName,
          filePath: item.filePath,
          value1: item.value1
        });
      }
    });

    me.top5_left.sort(function (a, b) {
      return parseFloat(b.value1) - parseFloat(a.value1);
    });
    var dataset = me.top5_left.slice(0, 5);

    dataset.forEach(function (d) {
      d.value1 = +d.value1;
    });

    var path = svg.selectAll('path')
      .data(pie(dataset))
      .enter()
      .append('path')
      .attr('d', arc)
      .style('fill', function (d, i) {
        return color(d.data.value1);
      });

    path.on('mouseover', function (d) {
      var total = d3.sum(dataset.map(function (d) {
        return d.value1;
      }));
      var percent = Math.round(1000 * d.data.value1 / total) / 10;
      tooltip.select('.projectName').html('project name:' + d.data.ProjectName);
      tooltip.select('.filePath').html('file path:' + d.data.filePath);
      tooltip.select('.value1').html('value1:' + d.data.value1);
      tooltip.style('display', 'block');
    });

    path.on('mouseout', function () {
      tooltip.style('display', 'none');
    });

    var legend = svg.selectAll('.legend')
      .data(color.domain())
      .enter()
      .append('g')
      .attr('class', 'legend')
      .attr('transform', function (d, i) {
        var height = legendRectSize + legendSpacing;
        var offset = height * color.domain().length / 2;
        var horz = -2 * legendRectSize;
        var vert = i * height - offset;
        return 'translate(' + horz + ',' + vert + ')';
      });

    legend.append('rect')
      .attr('width', legendRectSize)
      .attr('height', legendRectSize)
      .style('fill', color)
      .style('stroke', color);

    legend.append('text')
      .attr('x', legendRectSize + legendSpacing)
      .attr('y', legendRectSize - legendSpacing)
      .text(function (d) { return d; });
  }

  changeDate() {
    this.startDate = this.loginForm.value.DateRange.startDate;
    this.endDate = this.loginForm.value.DateRange.endDate;
    this.initChart();
  }
}
