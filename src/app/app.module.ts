import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginPageComponent } from './login-page/login-page.component';
import { GetDataService } from './_services/get-data.service';
import { AuthService } from './_services/auth.service';
import { DashBoardComponent } from './dash-board/dash-board.component';
import { AuthGuard } from './_guards/auth-guard.service';
import { NavbarComponent } from './navbar/navbar.component';
import { AuthInterceptor } from './shared/auth.interceptor';
import { TestsComponent } from './tests/tests.component';
import { DataContainerService } from './_services/data-container.service';
// import { Ng6O2ChartModule } from 'ng6-o2-chart';

import { NgxChartsModule } from '@swimlane/ngx-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    DashBoardComponent,
    NavbarComponent,
    TestsComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    // Ng6O2ChartModule
    NgxChartsModule,
    BrowserAnimationsModule
  ],
  providers: [
    GetDataService,
    AuthGuard,
    AuthService,
    DataContainerService,
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
