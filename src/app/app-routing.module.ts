import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { componentFactoryName } from '@angular/compiler';

import { LoginPageComponent } from './login-page/login-page.component';
import { DashBoardComponent } from './dash-board/dash-board.component';
import { TestsComponent } from './tests/tests.component';
import { AuthGuard } from './_guards/auth-guard.service';

const appRoutes: Routes = [
    { path: '', component: LoginPageComponent, pathMatch: 'full' },
    { path: 'DashBoard', component: DashBoardComponent, canActivate: [AuthGuard] },
    { path: 'Tests', component: TestsComponent }
  ];

  @NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [RouterModule]
  })

export class AppRoutingModule {
}
