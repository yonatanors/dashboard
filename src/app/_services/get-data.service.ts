import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Response, Headers, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { AggTimeByUniqueFile } from './_interfaces/data-formats';
import { DataContainerService } from './data-container.service';

@Injectable()
export class GetDataService {
  constructor(private http: HttpClient, private dataService: DataContainerService ) {}

  getData() {
    return this.http.get('http://localhost:3000/dashboard/bycompany')
      .map(result => result);
  }

  getTestData() {
    return this.http.get('http://localhost:3000/tests')
      .map(result => result);
  }

  getTestDataOne(startDate: Date, endDate: Date) {
    return this.http.post<AggTimeByUniqueFile>('http://localhost:3000/tests/one', { start_date: startDate, end_date: endDate})
      .map(data => this.dataService.setTotalTimeByFile(data));
  }
}
