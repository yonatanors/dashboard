import { Injectable, EventEmitter } from '@angular/core';

import { AggTimeByUniqueFile } from './_interfaces/data-formats';
import { GetDataService } from './get-data.service';

@Injectable()
export class DataContainerService {
  public totalTimeByFile: AggTimeByUniqueFile;
  public totalTimeByFileUpdated = new EventEmitter<AggTimeByUniqueFile>();

  constructor() {}

  getTotalTimeByFile() {
    return this.totalTimeByFile;
  }

  setTotalTimeByFile(data: AggTimeByUniqueFile) {
    this.totalTimeByFile = (data);
    this.totalTimeByFileUpdated.emit(this.totalTimeByFile);
  }

}


