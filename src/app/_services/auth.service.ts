import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';

import { LoginResponse } from './_interfaces/data-formats';


@Injectable()

export class AuthService {
    public isAuthenticated = false;
    public token: string;

    constructor(private httpClient: HttpClient) {}

    authUser(username: string, password: string) {
        return this.httpClient.post<LoginResponse>('http://localhost:3000/auth/login', { username: username, password: password })
            .subscribe(
                data => {
                    const expiresAt = moment().add(data.expiresIn, 'second').unix();
                    console.log(data.token);
                    localStorage.setItem('user_token', data.token);
                    localStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));
                },
                error => console.log(error)
            );
    }

    public logout() {
        localStorage.removeItem('user_token');
        localStorage.removeItem('expires_at');
    }

    public isLoggedIn(): boolean {
        const currDateTime = moment().unix();
        return moment(currDateTime).isBefore(this.getExpiration());
    }

    getExpiration() {
        const expiration = localStorage.getItem('expires_at');
        const expiresAt = JSON.parse(expiration);
        return moment(expiresAt);
    }
}
