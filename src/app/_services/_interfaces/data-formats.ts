import { Injectable } from '@angular/core';

export interface AggTimeByUniqueFile {
    fileName: string;
    filePath: string[];
    totalTime: number;
}

// export interface AggTimeByUniqueFile {
// AnalyzedDataArray: [{
//     fileName: string;
//     filePath: string[];
//     totalTime: number;
// }];
// }

export interface AggTimeByFolderLocation {
    objectName: string;
    totalTime: number;
}

export interface LoginResponse {
    userId: number;
    expiresIn: number;
    token: string;
}

@Injectable()
export abstract class DataFormatsService {
    abstract getAggTimeByUniqueFile(): AggTimeByUniqueFile[];
}
