import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { AuthService } from '../_services/auth.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  @ViewChild('f') loginForm: NgForm;
  userData = {
    username: '',
    password: ''
  };

  constructor(private authService: AuthService) {}

  ngOnInit() {
  }

  onSignIn() {
    this.authService.authUser(this.loginForm.value.userData.username, this.loginForm.value.userData.password);
    this.loginForm.reset();
  }

}
