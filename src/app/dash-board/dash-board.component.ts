import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';

import { GetDataService } from '../_services/get-data.service';
import { AuthService } from '../_services/auth.service';

@Component({
  selector: 'app-dash-board',
  templateUrl: './dash-board.component.html',
  styleUrls: ['./dash-board.component.css']
})
export class DashBoardComponent implements OnInit {
  chart1 = [];
  chart2 = [];
  chart3 = [];

  constructor(private _getData: GetDataService) { }

  ngOnInit() {
    this._getData.getData()
    .subscribe(result => {

      const fullFilePath = result['Data'].map(res => res.fullFilePath);
      const totalTimeSpent = result['Data'].map(res => res.totalTimeSpent);

      const fullFilePaths = [];
      fullFilePath.forEach((res) => {
        fullFilePaths.push(res);
      });
      this.chart1 = new Chart('canvas1', {
        type: 'pie',
        data: {
          labels: fullFilePaths,
          datasets: [
            {
              label: 'Time Spent Distribution',
              data: totalTimeSpent,
              backgroundColor: ['#3e95cd', '#8e5ea2', '#3cba9f', '#e8c3b9', '#c45850', '#5bc450'],
              fill: false
            }
          ]
        },
        options: {
          legend: {
            display: true
          },
          scales: {
            xAxes: [{
              display: false
            }],
            yAxes: [{
              display: false
            }]
          }
        }
      });
      this.chart2 = new Chart('canvas2', {
        type: 'pie',
        data: {
          labels: fullFilePaths,
          datasets: [
            {
              label: 'Time Spent Distribution',
              data: totalTimeSpent,
              backgroundColor: ['#3e95cd', '#8e5ea2', '#3cba9f', '#e8c3b9', '#c45850', '#5bc450'],
              fill: false
            }
          ]
        },
        options: {
          legend: {
            display: true
          },
          scales: {
            xAxes: [{
              display: false
            }],
            yAxes: [{
              display: false
            }]
          }
        }
      });
      this.chart3 = new Chart('canvas3', {
        type: 'line',
        data: {
          labels: fullFilePaths,
          datasets: [
            {
              label: 'Time Spent Distribution',
              data: totalTimeSpent,
              borderColor: '#3e95cd',
              fill: false
            }
          ]
        },
        options: {
          legend: {
            display: true
          },
          scales: {
            xAxes: [{
              display: true
            }],
            yAxes: [{
              display: true
            }]
          }
        }
      });
    });
  }

}
