import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs/observable';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    intercept(request: HttpRequest<any>,
              next: HttpHandler): Observable<HttpEvent<any>> {

        const Token = localStorage.getItem('user_token');

        if (Token) {
            const clonedReq = request.clone({
                setHeaders: {
                    Authorization: 'Bearer ' + Token
                }
            });
            console.log('intercepted!', clonedReq);
            return next.handle(clonedReq);
        } else {
            return next.handle(request);
        }
    }
}
