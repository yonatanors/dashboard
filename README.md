# The DashBoard Element
**This element contains the representational side of the project**
**The DashBoard should help the user gain insights, that would otherwise, be much harder to notice.**

## geting the environment ready
after downloading the code, run **npm install** to install all dependencies

## current pages
**login page** - sends credentials to backend server, and if correct, receives a token expires after one hour.
The interceptor (under- src->app->shared) addes this token to the header of future requests from the backend server.
The token is needed to get the JSON file from the backend server.

**DashBoard page** - currently works with charts.js, uses the **get-data.service** (under- src->app->_services) to retrieve JSON from backend server.
and creates 2 pie charts and a line graph using the retrieved data.

**Tests page** - I have used this page, to try and implement a data store service which would act as a local data store, and would reduce the
number of requests sent to the backend server. unfortunately, I did not have the time yet to finish it.
currently it receives a start date and a end date, and sends it to the backend server, which in turn sends back the data, if it exits in the database.

**Notes -**
There is a guard active on the DashBoard Page